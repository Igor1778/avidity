# GitHub Commits

## Introduction
This is an application I created to list commits of public GitHub repositories. After login, the user can enter `owner/repository` in the search bar, submit, and browse through a list of the latest commits in the default branch (usually master) of that repository.

## How to run it
- Clone the repository and `cd` into the `gh-commits` folder;
- Run the `yarn` command;
- Because `auth0` was installed using `npm install --save auth0-js`, you might have to run `npm install` as well;
- If everything went well, the app should launch in `http://localhost:3000`;
- In the Home screen, the will be the choice to straight to the Search page or to make the log in. If this is your first access, you will have to make the login using `auth0`. For testing purposes, I have used the email `ghcommitsavidity@gmail.com` and password `AVIDITY1234` to login;
- After login, you should be redirected to the Search page. Enter `owner/repository` in the search bar and click on the `Search` button to display commits.

## Technologies used
GitHub Commits was created using `create-react-app`. API requests were made using `axios` and the authentication was made using `auth0`.

## Aditional Info
If you have any trouble running the application, please send an email to `igor.wilbert@outlook.com`.

TEST
