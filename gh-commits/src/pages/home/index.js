import React, { Component } from "react";

import "./styles.css";

class Home extends Component {

  render() {
    return (
      <div className="home">
        <div className="home-title">Welcome to GitHub Commits!</div>
        <div className="cluster">
          <div className="home-card">
            <div className="login-text">Please login before using the search.</div>
            <div className="buttom-wrapper">
              <button className="buttom" onClick={this.props.auth.login}>Login</button>
            </div>
          </div>
          <div className="home-card">
            <div className="search-text">If already logged in, use the button below to go to the search.</div>
            <div className="buttom-wrapper">
              <a className="buttom" href="/search">Go To Search</a>
            </div>
          </div>
        </div>
      </div>
    );
  }
}


export default Home;