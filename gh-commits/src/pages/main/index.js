import React, { Component } from "react";
import CommitsList from "../../components/CommitsList";
import Search from "../../components/Search"
import Card from "../../components/Card"

import "./styles.css";

class Main extends Component {

  constructor(props) {
    super(props);

    this.state = {
      commits: [],
      repoInfo: {},
      repository: localStorage.getItem('last_repository')
    };
  }

  handleRepoInfo = (updatedRepoInfo) => {
    this.setState({ repoInfo: updatedRepoInfo })
  }

  handleCommits = (updatedCommits) => {
    this.setState({ commits: updatedCommits })
  }


  render() {
    return (
      <div className="main">
        <div className="main-title">
          <h1>Commit Search</h1>
        </div>
        <Search repository={this.state.repository} onUpdateRepoInfo={this.handleRepoInfo}
          onUpdateCommits={this.handleCommits} />
        <Card title={this.state.repoInfo.full_name} description={this.state.repoInfo.description} />
        <CommitsList commits={this.state.commits} />
      </div>
    );
  }
}


export default Main;