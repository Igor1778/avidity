import React, { Component } from "react";

import "./styles.css";

class ErrorPage extends Component {

  render() {
    return (
      <div>
        <div className="error-title">ERROR</div>
        <div className="error-body">
          <div className="error-message">User not found. Please go back to home and try to log in.</div>
          <a className="buttom" href="/">Go Back to Home</a>
        </div>
      </div>
    );
  }
}


export default ErrorPage;