import React, { Component } from "react";
import Auth from "../../auth";

import "./styles.css";

class Callback extends Component {

  componentDidMount() {
    const auth = new Auth();
    auth.handleAuthentication();
  }

  render() {
    return (<div className="callback-title">LOADING...</div>);
  }
}


export default Callback;