import React, { Component } from "react";
import api from "../../services/api";

import "./styles.css";


class Search extends Component {

  state = {
    repository: this.props.repository
  }

  loadCommits = async (isCommits) => {
    if (localStorage.getItem('last_repository') === null) {
      return;
    }

    const complement = isCommits ? "/commits" : "";
    try {
      const response = await api.get(`/repos/${this.state.repository}${complement}`);

      if (response) {
        if (isCommits) {
          this.props.onUpdateCommits(response.data);
        } else {
          this.props.onUpdateRepoInfo(response.data)
        }

      }
    } catch (err) {
      alert("Repository not found!");
    }
  }

  componentDidMount() {
    this.loadCommits(false);
    this.loadCommits(true);
  }


  handleInputChange = (e) => {
    this.setState({ repository: e.target.value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { repository } = this.state;

    if (repository === null || repository.length === 0) return;
    this.setState(
      () => {
        this.loadCommits(false);
        this.loadCommits(true);
        localStorage.setItem('last_repository', repository);
      }
    );
  }

  render() {
    return (

      <div className="search">
        <form onSubmit={this.handleSubmit}>
          <button id="submit" type="submit">Search</button>
          <input
            value={this.state.repository}
            onChange={this.handleInputChange}
            placeholder=" organization or username/repository">
          </input>
        </form>
      </div>
    );
  }
}

export default Search;