import React, { Component } from "react";

import Commit from "../../components/Commit";

import "./styles.css";


class CommitsList extends Component {
  render() {
    let hasCommits = this.props.commits.length !== 0;
    return (
      <div className="commits-list">
        <div className="commits-list-title">Commits:</div>
        {hasCommits ? this.props.commits.map(commitInfo => (
          <Commit authorName={commitInfo.commit.author.name}
            authorEmail={commitInfo.commit.author.email}
            date={commitInfo.commit.author.date}
            message={commitInfo.commit.message}
            sha={commitInfo.commit.tree.sha} />
        )
        ) : <div />}
      </div>
    )
  }
}

export default CommitsList;
