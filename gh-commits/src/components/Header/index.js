import React, { Component } from "react";

import "./styles.css";


class Header extends Component {
  render() {
    return (
      <header id="main-header">GitHub Commits</header>
    );
  }
}

export default Header;