import React, { Component } from "react";

import "./styles.css";


class Commit extends Component {

  formatDate(date) {
    if (date === null || date.length === 0) return;

    let day = date.slice(0, 10);
    let hour = date.slice(12, 19);
    return day + " " + hour;
  }

  render() {
    return (
      <article className="commit" key={this.props.id}>
        <div className="author-info">
          <div>Author: {this.props.authorName}</div>
          <div>E-mail: {this.props.authorEmail}</div>
        </div>
        <div className="message-title">Message:</div>
        <div className="message-body">{this.props.message}</div>
        <div className="bottom-info">
          <div>Commit SHA: {this.props.sha}</div>
          <div>Date: {this.formatDate(this.props.date)}</div>
        </div>
      </article>
    );
  }
}

export default Commit;