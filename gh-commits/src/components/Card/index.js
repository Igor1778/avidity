import React, { Component } from "react";

import "./styles.css";


class Card extends Component {
  render() {
    return (
      <div className="card">
        <div className="card-info">
          <div className="info-title">Name: </div>
          <div>{this.props.title}</div>
        </div>
        <div className="card-info">
          <div className="info-title no-padding-bottom">Description: </div>
          <div>{this.props.description}</div>
        </div>
      </div>
    );
  }
}

export default Card;