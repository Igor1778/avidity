/* eslint no-restricted-globals: 0*/
import auth0 from "auth0-js";


const LOGIN_SUCCESS_PAGE = "/search";
const LOGIN_ERROR_PAGE = "/error"

class Auth {
    auth0 = new auth0.WebAuth({
        domain: "dev-5gn0vj8b.auth0.com",
        clientID: "EIix8fHDstLhytKctoKkfNXh5jCusddq",
        redirectUri: "http://localhost:3000/callback",
        responseType: "token id_token",
        scope: "openid"
    });

    constructor() {
        this.login = this.login.bind(this);
    }

    login() {
        this.auth0.authorize();
    }


    handleAuthentication() {
        this.auth0.parseHash((err, authResults) => {
            if (authResults && authResults.accessToken && authResults.idToken) {
                let expiresAt = JSON.stringify((authResults.expiresIn) * 1000 + new Date().getTime());
                localStorage.setItem("access_token", authResults.accessToken);
                localStorage.setItem("id_token", authResults.idToken);
                localStorage.setItem("expires_at", expiresAt);
                location.hash = ""
                location.pathname = LOGIN_SUCCESS_PAGE;
            } else {
                location.pathname = LOGIN_ERROR_PAGE;
                console.log(err);
            }
        })
    }

    isAuthenticated() {
        let expiresAt = JSON.parse(localStorage.getItem("expires_at"));
        let result = new Date().getTime() < expiresAt;
        return result
    }
}


export default Auth;