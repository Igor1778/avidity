import React, { Component } from "react";

import Main from "./pages/main";
import Home from "./pages/home";
import ErrorPage from "./pages/errorPage";
import Callback from "./pages/callback";


class Routes extends Component {
  render() {
    let componentToRender = "";

    switch (this.props.location) {
      case "":
        componentToRender = <Home auth={this.props.auth} />
        break;
      case "callback":
        componentToRender = <Callback />
        break;
      case "search":
        componentToRender = this.props.auth.isAuthenticated() ? <Main /> : <ErrorPage />
        break;
      case "error":
        componentToRender = <ErrorPage />
        break;
      default:
        componentToRender = <Home auth={this.props.auth} />
    };

    return (<div>{componentToRender}</div>);
  }
}

export default Routes;