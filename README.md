# Avidity Challenge

For this challenge, I made an app called GitHub Commits, that hopefully meets all the requirements given.

## How to run GitHub Commits
- Clone the repository and `cd` into the `gh-commits` folder;
- Run the `yarn` command;
- Because `auth0` was installed using `npm install --save auth0-js`, you might have to run `npm install` as well;
- If everything went well, the app should launch in `http://localhost:3000`;
- In the Home screen, the will be the choice to straight to the Search page or to make the log in. If this is your first access, you will have to make the login using `auth0`. For testing purposes, I have used the email `ghcommitsavidity@gmail.com` and password `AVIDITY1234` to login;
- After login, you should be redirected to the Search page. Enter `owner/repository` in the search bar and click on the `Search` button to display commits.

## Approximate time spent on the project
- 19/07/19: 40 minutes

- 20/07/19: 5 hours and 50 minutes.

- 21/07/19: 6 hours and 35 minutes